require('source-map-support/register');
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/api/mangas/routes/mangas-routes.js":
/*!************************************************!*\
  !*** ./src/api/mangas/routes/mangas-routes.js ***!
  \************************************************/
/*! exports provided: mangaRouter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mangaRouter", function() { return mangaRouter; });
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _models_mangas_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/mangas-model */ "./src/api/models/mangas-model.js");

 // permet de d'interagire avec mongodb (orm)

var mangaRouter = express__WEBPACK_IMPORTED_MODULE_0___default.a.Router();
mangaRouter
/** Collection opérations */
.get('/', function (req, res) {
  // GET request
  // on récupére tout les mangas avec la method find
  _models_mangas_model__WEBPACK_IMPORTED_MODULE_1__["Mangas"].find({}, function (err, mangas) {
    if (err) return res.status(err);
    return res.json(mangas);
  });
}).post('/', function (req, res) {
  // POST request
  // on modifie le titre pour enlever les espace
  var newManga = new _models_mangas_model__WEBPACK_IMPORTED_MODULE_1__["Mangas"](req.body); // On créer un nouveau manga
  //on persist le manga en bdd avec le method save

  newManga.save(function (err, manga) {
    if (err) return res.status(400).json(err);
    return res.json({
      message: "success",
      manga: manga
    });
  });
})
/** Items opérations */
.get("/:id", function (req, res) {
  // GET request
  _models_mangas_model__WEBPACK_IMPORTED_MODULE_1__["Mangas"].findById(req.params.id, function (err, manga) {
    if (err) return res.status(400).json(err);
    return res.json({
      message: "success",
      manga: manga
    });
  });
}).put('/:id', function (req, res) {
  // PUT request
  _models_mangas_model__WEBPACK_IMPORTED_MODULE_1__["Mangas"].findById(req.params.id, function (err, manga) {
    if (err) return res.json(err);
    Object.assign(manga, req.body).save(function (err, manga) {
      if (err) return res.json(err);
      return res.json({
        message: "success",
        manga: manga
      });
    });
  });
})["delete"]('/:id', function (req, res) {
  // DELETE request
  _models_mangas_model__WEBPACK_IMPORTED_MODULE_1__["Mangas"].findByIdAndDelete(req.params.id, function (err, result) {
    if (err) return res.json(err);
    return res.json({
      message: 'manga supprimer avec success',
      result: result
    });
  });
});


/***/ }),

/***/ "./src/api/models/mangas-model.js":
/*!****************************************!*\
  !*** ./src/api/models/mangas-model.js ***!
  \****************************************/
/*! exports provided: Mangas */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Mangas", function() { return Mangas; });
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongoose */ "mongoose");
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);


var titleSmallValidator = function titleSmallValidator(v) {
  return v.length > 5;
};

var titleBigValidator = function titleBigValidator(v) {
  return v.length < 80;
};

var mangaSchema = new mongoose__WEBPACK_IMPORTED_MODULE_0__["Schema"]({
  title: {
    type: String,
    validate: [{
      validator: titleSmallValidator,
      msg: 'le titre et tros cour, il doit faire minimum 5 charactère'
    }, {
      validator: titleBigValidator,
      msg: 'le titre et tros grand, maximum 80 charactère'
    }],
    required: true,
    index: true,
    unique: true
  },
  author: {
    type: String,
    max: 50,
    min: 3,
    required: true
  },
  gender: {
    type: String,
    max: 50,
    min: 3,
    required: true
  },
  pages: {
    type: Number,
    min: 30,
    max: 10000,
    required: true
  },
  addAt: {
    type: Date,
    "default": Date.now
  }
});
var Mangas = mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.model('Managas', mangaSchema);


/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: app */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "app", function() { return app; });
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! config */ "config");
/* harmony import */ var config__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(config__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! mongoose */ "mongoose");
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _api_mangas_routes_mangas_routes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./api/mangas/routes/mangas-routes */ "./src/api/mangas/routes/mangas-routes.js");
/* harmony import */ var volleyball__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! volleyball */ "volleyball");
/* harmony import */ var volleyball__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(volleyball__WEBPACK_IMPORTED_MODULE_4__);





/** Connexion à la base donnée mongodb */

mongoose__WEBPACK_IMPORTED_MODULE_2___default.a.connect(config__WEBPACK_IMPORTED_MODULE_1___default.a.db, {
  useNewUrlParse: true
});
var db = mongoose__WEBPACK_IMPORTED_MODULE_2___default.a.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function () {
  console.log('connection database ok!');
});
/** server web express */

var app = express__WEBPACK_IMPORTED_MODULE_0___default()();
/** Middleware */

if (config__WEBPACK_IMPORTED_MODULE_1___default.a.util.getEnv("NODE_ENV") !== 'test') {
  app.use(volleyball__WEBPACK_IMPORTED_MODULE_4___default.a);
}

app.use(express__WEBPACK_IMPORTED_MODULE_0___default.a.json());
app.use(express__WEBPACK_IMPORTED_MODULE_0___default.a.urlencoded({
  extended: true
}));
/** Routes */

app.use('/mangas', _api_mangas_routes_mangas_routes__WEBPACK_IMPORTED_MODULE_3__["mangaRouter"]);
/** API routes ==> Mangas */

app.listen(config__WEBPACK_IMPORTED_MODULE_1___default.a.PORT, function () {
  console.log("app listening at localhost:".concat(config__WEBPACK_IMPORTED_MODULE_1___default.a.PORT));
});
 // pour tester

/***/ }),

/***/ 0:
/*!****************************!*\
  !*** multi ./src/index.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/nils/code/nodejs/manga/src/index.js */"./src/index.js");


/***/ }),

/***/ "config":
/*!*************************!*\
  !*** external "config" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("config");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "mongoose":
/*!***************************!*\
  !*** external "mongoose" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),

/***/ "volleyball":
/*!*****************************!*\
  !*** external "volleyball" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("volleyball");

/***/ })

/******/ });
//# sourceMappingURL=main.map