import mongoose, { Schema } from "mongoose"

const titleSmallValidator = (v) => {
    return v.length > 5
} 

const titleBigValidator = (v) => {
    return v.length < 80
} 

const mangaSchema = new Schema({
    title: {
        type: String, 
        validate: [
            {validator: titleSmallValidator, msg: 'le titre et tros cour, il doit faire minimum 5 charactère'},
            {validator: titleBigValidator, msg: 'le titre et tros grand, maximum 80 charactère'},
        ],
        required: true,
        index: true,
        unique: true,
    },
    author: {type: String, required: true},
    gender: {type: String, required: true},
    pages: {type: Number, min: 30, max: 10000, required: true},
    addAt: {type: Date, default: Date.now}
})

const Mangas = mongoose.model('Managas', mangaSchema)

export { Mangas };