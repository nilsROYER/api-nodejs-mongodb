import express from 'express'
import { Mangas } from '../../models/mangas-model' // permet de d'interagire avec mongodb (orm)

const mangaRouter = express.Router()

mangaRouter
    /** Collection opérations */
    .get('/', (req, res) => { // GET request
        // on récupére tout les mangas avec la method find
        Mangas.find({}, (err, mangas) => {  
            if(err) return res.status(err)
            return res.status(200).json(mangas)
        })
    })
    .post('/', (req, res) => { // POST request
        // on modifie le titre pour enlever les espace
        let newManga = new Mangas(req.body) // On créer un nouveau manga
        //on persist le manga en bdd avec le method save
        newManga.save((err, manga) => { 
            if(err) return res.status(400).json(err)
            return res.status(200).json({message: "Manga créer avec succès!", manga})
        })
    })
    /** Items opérations */
    .get("/:id", (req, res) => { // GET request
        Mangas.findById(req.params.id, (err, manga) => {
            if(err) return res.status(400).json(err)
            return res.json({message: "Manga trouvé!", manga})
        })
    })
    .put('/:id', (req, res) => { // PUT request
        Mangas.findById(req.params.id, (err, manga) => {
            if(err) return resstatus(400).json(err)
            Object.assign(manga, req.body).save((err, manga) => {
                if(err) return res.status(400).json(err)
                return res.status(200).json({message: "Manga modifier avec succès!", manga})
            })
        })
    })
    .delete('/:id', (req, res) => { // DELETE request
        Mangas.findByIdAndDelete(req.params.id, (err, result) => {
            if(err) return res.status(400).json(err)
            return res.status(200).json({message: 'Manga supprimé avec succès!', result})
        })
    })













export {mangaRouter}