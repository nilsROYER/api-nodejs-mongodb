import express from 'express'
import config from 'config'
import mongoose from 'mongoose'
import { mangaRouter } from './api/mangas/routes/mangas-routes'
import volleyball from 'volleyball'

/** Connexion à la base donnée mongodb */
mongoose.connect(config.db, {useNewUrlParser: true, useUnifiedTopology: true})
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error'))
db.once('open', () => {
    console.log('connection database ok!')
}) 


/** server web express */
const app = express()

/** Middleware */
if(config.NODE_ENV !== 'test'){
    app.use(volleyball)
}

app.use(express.json())
app.use(express.urlencoded({extended: true}))

/** Routes */
app.use('/mangas', mangaRouter)/** API routes ==> Mangas */

app.listen(config.PORT, () => {
    console.log(`app listening at localhost:${config.PORT}`)
})

export {app} // pour tester