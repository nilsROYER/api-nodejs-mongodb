import { Mangas } from '../src/api/models/mangas-model'

import chai from 'chai' // librairie de test
import { app } from '../src/index' // mon server node

const chaiHttp = require("chai-http") // plugins chai pour faire des requetes http utile dans le cadre d'une api
chai.should()
chai.use(chaiHttp)

describe("Mangas", () => {
    /** Prération des tests, on vide la bdd avant de lancer les test */
    beforeEach(done => {
        Mangas.remove({}, err => {
            done()
        })
    })

    describe("/GET Mangas", () => {
        it("should GET return all mangas", done => {
            let mangas = [
                new Mangas({
                    title: "One piece",
                    author: 'Eichiro Oda',
                    gender: 'shonen',
                    pages: 2000
                }),
                new Mangas({
                    title: "Bleach",
                    author: 'Tite kubo',
                    gender: 'shonen',
                    pages: 1500
                })
            ]
            mangas[1].save(() => {return})
            mangas[0].save(() => {
                chai
                .request(app)
                .get("/mangas")
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('array')
                    res.body.length.should.be.eql(2)

                    done()
                })
            })
        })
        it("should GET return one manga", done => {
            let manga =  new Mangas({
                title: "One piece",
                author: 'Eichiro Oda',
                gender: 'shonen',
                pages: 2000
            })
            manga.save((err, manga) => {
                chai
                .request(app)
                .get(`/mangas/${manga.id}`)
                .send(manga)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.manga.should.have.property("title").eql(manga.title)
                    res.body.manga.should.have.property("author").eql(manga.author)
                    res.body.manga.should.have.property("gender").eql(manga.gender)
                    res.body.manga.should.have.property("pages").eql(manga.pages)
                    res.body.manga.should.have.property("addAt")

                    done()
                })
            })
        })
            
    })

    describe("/POST Manga", () => {
        it("should POST a manga", done => {
            let manga = {
                title: 'One piece',
                author: 'Eichiro Oda',
                gender: 'Shonen',
                pages: 2000
            }

            chai
                .request(app)
                .post('/mangas')
                .send(manga)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.have.property("message")
                        .eql("Manga créer avec succès!")
                    res.body.manga.should.have.property("title")
                    res.body.manga.should.have.property("author")
                    res.body.manga.should.have.property("gender")
                    res.body.manga.should.have.property("pages")
                    res.body.manga.should.have.property("addAt")
                    done()
                })
        })
        it("should POST a manga but the title is to small", done => {
            let manga = {
                title: 'On',
                author: 'Eichiro Oda',
                gender: 'Shonen',
                pages: 2000
            }

            chai
                .request(app)
                .post('/mangas')
                .send(manga)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.be.an('object')
                    res.body.should.have.property("errors")
                    res.body.errors.should.have.property("title")
                    res.body.errors.title.should.have.property('message')
                        .eql('le titre et tros cour, il doit faire minimum 5 charactère')

                    done()
                })
        })
        it("should POST a manga but the title is to big", done => {
            let manga = {
                title: 'Onnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn',
                author: 'Eichiro Oda',
                gender: 'Shonen',
                pages: 2000
            }

            chai
                .request(app)
                .post('/mangas')
                .send(manga)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.be.an('object')
                    res.body.should.have.property("errors")
                    res.body.errors.should.have.property("title")
                    res.body.errors.title.should.have.property('message')
                        .eql('le titre et tros grand, maximum 80 charactère')

                    done()
                })
        })
        it("should POST a manga but he has no title", done => {
            let manga = {
                author: 'Eichiro Oda',
                gender: 'Shonen',
                pages: 2000
            }

            chai
                .request(app)
                .post('/mangas')
                .send(manga)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.be.an('object')
                    res.body.should.have.property("errors")
                    res.body.errors.should.have.property("title")
                    res.body.errors.title.should.have.property('kind')
                        .eql('required')

                    done()
                })
        })
        it("should POST a manga but he has no author", done => {
            let manga = {
                title: "One piece",
                gender: 'Shonen',
                pages: 2000
            }

            chai
                .request(app)
                .post('/mangas')
                .send(manga)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.be.an('object')
                    res.body.should.have.property("errors")
                    res.body.errors.should.have.property("author")
                    res.body.errors.author.should.have.property('kind')
                        .eql('required')

                    done()
                })
        })
        it("should POST a manga but he has no gender", done => {
            let manga = {
                title: 'One piece',
                author: 'Eichiro Oda',
                pages: 2000
            }

            chai
                .request(app)
                .post('/mangas')
                .send(manga)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.be.an('object')
                    res.body.should.have.property("errors")
                    res.body.errors.should.have.property("gender")
                    res.body.errors.gender.should.have.property('kind')
                        .eql('required')

                    done()
                })
        })
        it("should POST a manga but he has no pages", done => {
            let manga = {
                title: 'One piece',
                author: 'Eichiro Oda',
                gender: "shonen"
            }

            chai
                .request(app)
                .post('/mangas')
                .send(manga)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.be.an('object')
                    res.body.should.have.property("errors")
                    res.body.errors.should.have.property("pages")
                    res.body.errors.pages.should.have.property('kind')
                        .eql('required')

                    done()
                })
        })
        it("should POST a manga but there are not many pages", done => {
            let manga = {
                title: 'One piece',
                author: 'Eichiro Oda',
                gender: "shonen",
                pages: 20
            }

            chai
                .request(app)
                .post('/mangas')
                .send(manga)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.be.an('object')
                    res.body.should.have.property("errors")
                    res.body.errors.should.have.property("pages")
                    res.body.errors.pages.should.have.property('kind')
                        .eql('min')
                    res.body.errors.pages.properties.should.have.property("min")
                        .eql(Mangas.schema.obj.pages.min)

                    done()
                })
        })
        it("should POST a manga but there are many pages", done => {
            let manga = {
                title: 'One piece',
                author: 'Eichiro Oda',
                gender: "shonen",
                pages: 20000
            }

            chai
                .request(app)
                .post('/mangas')
                .send(manga)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.be.an('object')
                    res.body.should.have.property("errors")
                    res.body.errors.should.have.property("pages")
                    res.body.errors.pages.should.have.property('kind')
                        .eql('max')
                    res.body.errors.pages.properties.should.have.property("max")
                        .eql(Mangas.schema.obj.pages.max)

                    done()
                })
        })
    })
    
    describe("/PUT manga", () => {
        it("should update a manga given the id", done => {
            let manga = new Mangas({
                title: 'One piece',
                author: 'Eichiro Oda',
                gender: "shonen",
                pages: 2000
            })
            manga.save((err, manga) => {
                chai.request(app)
                    .put(`/mangas/${manga.id}`)
                    .send({
                        title: 'One piece',
                        author: 'Tite Kubo',
                        gender: "shonen",
                        pages: 2000
                    })
                    .end((err, res) => {
                        res.should.have.status(200)
                        res.body.should.be.an("object")
                        res.body.should.have.property("message").eql("Manga modifier avec succès!")
                        res.body.manga.should.have.property("author")
                            .eql("Tite Kubo")

                        done()
                    })
            })
        })
    })
    describe("/DELETE manga", () => {
        it("should delte manga given the id", done => {
            let manga = new Mangas({
            title: 'One piece',
            author: 'Eichiro Oda',
            gender: "shonen",
            pages: 2000
            })
            manga.save((err, manga) => {
                chai
                    .request(app)
                    .delete(`/mangas/${manga.id}`)
                    .end((err, res) => {
                        res.should.have.status(200)
                        res.body.should.be.an('object')
                        res.body.should.have.property('message')
                            .eql('Manga supprimé avec succès!')

                        done()
                    })
            })
        })
        
    })
})
